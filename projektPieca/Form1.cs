﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace projektPieca
{
    public partial class Piecyk : Form
    {
        int tabSize = 100;
        float Wyjscie;
        float WartoscOziebiania;
        int randLiczba;

        public Piecyk()
        {
            InitializeComponent();
        }

        private void temperaturaPokojuBar_Scroll(object sender, EventArgs e)
        {
            aktualnaTempPok.Text = "" + temperaturaPokojuBar.Value + "°C";
        }

        public async Task GeneratorTemp_Zima()
        {
            int wczytaj;
            bool TemperaturaBool;
            int[] tablicaTemperatur = File.ReadLines(@"Zima.txt")
                .Select(n =>
                {
                    TemperaturaBool = int.TryParse(n,out wczytaj);
                    return new { wczytaj, TemperaturaBool };
                })
                .Where(x => x.TemperaturaBool)
                .Select(x => x.wczytaj).ToArray();
            for (int i = 1; i < tabSize; i++)
            {
                await Task.Delay(1000);
                aktualnaTempZew.Text = tablicaTemperatur[i].ToString();
                Rozmycie();
            }
        }

        public async Task GeneratorTemp_Wiosna()
        {
            int wczytaj;
            bool TemperaturaBool;
            int[] tablicaTemperatur = File.ReadLines(@"Wiosna.txt")
                .Select(n =>
                {
                    TemperaturaBool = int.TryParse(n, out wczytaj);
                    return new { wczytaj, TemperaturaBool };
                })
                .Where(x => x.TemperaturaBool)
                .Select(x => x.wczytaj).ToArray();
            for (int i = 1; i < tabSize; i++)
            {
                await Task.Delay(1000);
                aktualnaTempZew.Text = tablicaTemperatur[i].ToString();
                Rozmycie();
            }
        }

        public async Task GeneratorTemp_Lato()
        {
            int wczytaj;
            bool TemperaturaBool;
            int[] tablicaTemperatur = File.ReadLines(@"Lato.txt")
                .Select(n =>
                {
                    TemperaturaBool = int.TryParse(n, out wczytaj);
                    return new { wczytaj, TemperaturaBool };
                })
                .Where(x => x.TemperaturaBool)
                .Select(x => x.wczytaj).ToArray();
            for (int i = 1; i < tabSize; i++)
            {
                await Task.Delay(1000);
                aktualnaTempZew.Text = tablicaTemperatur[i].ToString();
                Rozmycie();
            }
        }

        public async Task GeneratorTemp_Jesien()
        {
            int wczytaj;
            bool TemperaturaBool;
            int[] tablicaTemperatur = File.ReadLines(@"Jesien.txt")
                .Select(n =>
                {
                    TemperaturaBool = int.TryParse(n, out wczytaj);
                    return new { wczytaj, TemperaturaBool };
                })
                .Where(x => x.TemperaturaBool)
                .Select(x => x.wczytaj).ToArray();
            for (int i = 1; i < tabSize; i++)
            {
                await Task.Delay(1000);
                aktualnaTempZew.Text = tablicaTemperatur[i].ToString();
                Rozmycie();
            }
        }

        public async Task GeneratorTemp_Stala()
        {
            int tablicaTemperatur = 20;
            
            for (int i = 1; i < 100; i++)
            {
                await Task.Delay(1000);
                aktualnaTempZew.Text = tablicaTemperatur.ToString();
                Rozmycie();
            }
        }

        public void Rozmycie()
        {
            int TemZew = Convert.ToInt32(aktualnaTempZew.Text);
            int TempBar = Convert.ToInt32(temperaturaPokojuBar.Value);
            float temperatura = TempBar - Wyjscie;

            float uZimno = 0.00f;
            float uChlodno = 0.00f;
            float uLetnio = 0.00f;
            float uCieplo = 0.00f;
            float uGoraco = 0.00f;
            float DanaTemp = 0.00f;
            DanaTemp = temperaturaPokojuBar.Value;

            // Zimno 
            if (temperatura >= 40.00f)
            {
                uZimno = 1.00f;
            }
            else if (temperatura >= 32.00f && temperatura <= 34.00f)
            {
                uZimno = (34.00f - temperatura) / (34.00f - 32.00f);
            }
            else
            {
                uZimno = 0.00f;
            }

            // Chlodno 
            if (temperatura >= 32.00f && temperatura <= 34.00f)
            {
                uChlodno = (temperatura - 34.00f) / (32.00f - 34.00f);
            }
            else if (temperatura >= 26.00f && temperatura <= 32.00f)
            {
                uChlodno = 1.00f;
            }
            else if (temperatura >= 24.00f && temperatura < 26.00f)
            {
                uChlodno = (24.00f - temperatura) / (24.00f - 26.00f);
            }
            else
            {
                uChlodno = 0.00f;
            }

            // Letnio 
            if (temperatura >= 24.00f && temperatura <= 26.00f)
            {
                uLetnio = (temperatura - 26.00f) / (24.00f - 26.00f);
            }
            else if (temperatura >= 18.00f && temperatura <= 24.00f)
            {
                uLetnio = 1.00f;
            }
            else if (temperatura >= 16.00f && temperatura < 18.00f)
            {
                uLetnio = (16.00f - temperatura) / (16.00f - 18.00f);
            }
            else
            {
                uLetnio = 0.00f;
            }

            // Cieplo 
            if (temperatura >= 16.00f && temperatura <= 18.00f)
            {
                uCieplo = (temperatura - 18.00f) / (16.00f - 18.00f);
            }
            else if (temperatura >= 10.00f && temperatura <= 16.00f)
            {
                uCieplo = 1.00f;
            }
            else if (temperatura >= 8.00f && temperatura < 10.00f)
            {
                uCieplo = (8.00f - temperatura) / (8.00f - 10.00f);
            }
            else
            {
                uCieplo = 0.00f;
            }

            // Goraco 
            if (temperatura >= 8.00f && temperatura <= 10.00f)
            {
                uGoraco = (temperatura - 10.00f) / (8.00f - 10.00f);
            }
            else if (temperatura >= 0.00f && temperatura <= 8.00f)
            {
                uGoraco = 1.00f;
            }
            else
            {
                uGoraco = 0.00f;
            }

            System.Console.WriteLine("uZimno: " + uZimno + "; uChlodno :" + uChlodno + "; uLetnio :" + uLetnio + "; uCieplo :" + uCieplo + "; uGoraco :" + uGoraco);

            // Wnioskowanie

            float SWu = 0.00f;
            float sW = 0.00f;

            float A = System.Math.Min(uZimno, DanaTemp);
            float B = System.Math.Min(uChlodno, DanaTemp);
            float C = System.Math.Min(uLetnio, DanaTemp);
            float D = System.Math.Min(uCieplo, DanaTemp);
            float E = System.Math.Min(uGoraco, DanaTemp);


            SWu = uZimno * A + uChlodno * B + uLetnio * C + uCieplo * D + uGoraco * E;
            sW = A + B + C + D + E + 0.01f;
            //Wyostrzenie

            float wyostrzenie = 0.00f;
            wyostrzenie = (SWu / sW);

            Grzanie(wyostrzenie, DanaTemp);
            oziebianie(Wyjscie, TemZew);
            WykresTwew(); 
        }
        public void oziebianie(float Wyjscie, int TemZew)
        {
            Random rnd = new Random();
            float liczbaLosowa = rnd.Next(50, randLiczba);
            if (Wyjscie > TemZew)
            {
                Wyjscie = Wyjscie - (liczbaLosowa / WartoscOziebiania);
            }
            else
            {
                Wyjscie = Wyjscie + (liczbaLosowa / 100.00f);
            }
            aktualnaTempWew.Text = Wyjscie.ToString();
            System.Console.WriteLine("Temp: " + aktualnaTempWew.Text);
            richTextBox1.AppendText("Temperatura pokoju: " + Wyjscie + "\n");
        }

        public void Grzanie(float wyostrzenie, float DanaTemp)
        {
            wyostrzenie = wyostrzenie / 100f;
            float value = 0f;
            if (Wyjscie < DanaTemp)
            {
                value = DanaTemp - wyostrzenie;
                Wyjscie += value / 10f;
            }
            else
            {
                wyostrzenie = (Wyjscie - DanaTemp) - wyostrzenie;
                Wyjscie -= (wyostrzenie +0.01f)/ 10f;
            }
            
        }

        public void WykresTwew()
        {
            float x = float.Parse(aktualnaTempWew.Text); 
            float x2 = float.Parse(aktualnaTempZew.Text);
            int y = 0;
            if (50 > x)
            {
                
                aTempWykres.Series["TabelaWew"].Points.AddXY(y, x);
                aTempZewWykres.Series["TabelaZew"].Points.AddXY(y, x2);
            }
        }

        //------------------------------------------------------------------- CheckBox'y
        public void wiosnaBox_CheckedChanged(object sender, EventArgs e)
        {
            GeneratorTemp_Wiosna();
        }

        public void latoBox_CheckedChanged(object sender, EventArgs e)
        {
            GeneratorTemp_Lato();
        }

        public void jesienBox_CheckedChanged(object sender, EventArgs e)
        {
            GeneratorTemp_Jesien();
        }

        public void zimaBox_CheckedChanged(object sender, EventArgs e)
        {
            GeneratorTemp_Zima();
        }

        private void stalaBox_CheckedChanged(object sender, EventArgs e)
        {
            GeneratorTemp_Stala();
        }

        private void OcieplanyBox_CheckedChanged(object sender, EventArgs e)
        {
            if (OcieplanyBox.Checked == true)
            {
                 WartoscOziebiania = 100.00f;
                randLiczba = 100;
            }
            else
            {
                 WartoscOziebiania = 110.00f;
                randLiczba = 100;
            }
        }

        private void NieOcieplanyBox_CheckedChanged(object sender, EventArgs e)
        {
            if (NieOcieplanyBox.Checked == true)
            {
                WartoscOziebiania = 1500.00f;
                randLiczba = 250;
            }
            else
            {
                WartoscOziebiania = 110.00f;
                randLiczba = 100;
            }
        }

        //------------------------------------------------------------------

    }
}


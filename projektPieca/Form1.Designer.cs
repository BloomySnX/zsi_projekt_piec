﻿namespace projektPieca
{
    partial class Piecyk
    {
        /// <summary>
        /// Wymagana zmienna projektanta.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Wyczyść wszystkie używane zasoby.
        /// </summary>
        /// <param name="disposing">prawda, jeżeli zarządzane zasoby powinny zostać zlikwidowane; Fałsz w przeciwnym wypadku.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Kod generowany przez Projektanta formularzy systemu Windows

        /// <summary>
        /// Metoda wymagana do obsługi projektanta — nie należy modyfikować
        /// jej zawartości w edytorze kodu.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataVisualization.Charting.ChartArea chartArea5 = new System.Windows.Forms.DataVisualization.Charting.ChartArea();
            System.Windows.Forms.DataVisualization.Charting.Legend legend5 = new System.Windows.Forms.DataVisualization.Charting.Legend();
            System.Windows.Forms.DataVisualization.Charting.Series series5 = new System.Windows.Forms.DataVisualization.Charting.Series();
            System.Windows.Forms.DataVisualization.Charting.ChartArea chartArea6 = new System.Windows.Forms.DataVisualization.Charting.ChartArea();
            System.Windows.Forms.DataVisualization.Charting.Legend legend6 = new System.Windows.Forms.DataVisualization.Charting.Legend();
            System.Windows.Forms.DataVisualization.Charting.Series series6 = new System.Windows.Forms.DataVisualization.Charting.Series();
            this.temperaturaPokojuBar = new System.Windows.Forms.TrackBar();
            this.tempPokText = new System.Windows.Forms.TextBox();
            this.aktualnaTempPok = new System.Windows.Forms.Label();
            this.richTextBox1 = new System.Windows.Forms.RichTextBox();
            this.wiosnaBox = new System.Windows.Forms.CheckBox();
            this.latoBox = new System.Windows.Forms.CheckBox();
            this.jesienBox = new System.Windows.Forms.CheckBox();
            this.zimaBox = new System.Windows.Forms.CheckBox();
            this.tempZewText = new System.Windows.Forms.TextBox();
            this.aktualnaTempZew = new System.Windows.Forms.Label();
            this.tempWewText = new System.Windows.Forms.TextBox();
            this.aktualnaTempWew = new System.Windows.Forms.Label();
            this.stalaBox = new System.Windows.Forms.CheckBox();
            this.aTempWykres = new System.Windows.Forms.DataVisualization.Charting.Chart();
            this.aTempZewWykres = new System.Windows.Forms.DataVisualization.Charting.Chart();
            this.OcieplanyBox = new System.Windows.Forms.CheckBox();
            this.NieOcieplanyBox = new System.Windows.Forms.CheckBox();
            ((System.ComponentModel.ISupportInitialize)(this.temperaturaPokojuBar)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.aTempWykres)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.aTempZewWykres)).BeginInit();
            this.SuspendLayout();
            // 
            // temperaturaPokojuBar
            // 
            this.temperaturaPokojuBar.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.temperaturaPokojuBar.Location = new System.Drawing.Point(441, 11);
            this.temperaturaPokojuBar.Maximum = 40;
            this.temperaturaPokojuBar.Name = "temperaturaPokojuBar";
            this.temperaturaPokojuBar.Orientation = System.Windows.Forms.Orientation.Vertical;
            this.temperaturaPokojuBar.Size = new System.Drawing.Size(45, 234);
            this.temperaturaPokojuBar.TabIndex = 0;
            this.temperaturaPokojuBar.Scroll += new System.EventHandler(this.temperaturaPokojuBar_Scroll);
            // 
            // tempPokText
            // 
            this.tempPokText.BackColor = System.Drawing.Color.WhiteSmoke;
            this.tempPokText.Enabled = false;
            this.tempPokText.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F);
            this.tempPokText.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.tempPokText.Location = new System.Drawing.Point(492, 11);
            this.tempPokText.Name = "tempPokText";
            this.tempPokText.Size = new System.Drawing.Size(167, 23);
            this.tempPokText.TabIndex = 1;
            this.tempPokText.Text = "Temperatura pokoju";
            // 
            // aktualnaTempPok
            // 
            this.aktualnaTempPok.AutoSize = true;
            this.aktualnaTempPok.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F);
            this.aktualnaTempPok.Location = new System.Drawing.Point(665, 14);
            this.aktualnaTempPok.Name = "aktualnaTempPok";
            this.aktualnaTempPok.Size = new System.Drawing.Size(31, 17);
            this.aktualnaTempPok.TabIndex = 4;
            this.aktualnaTempPok.Text = "0°C";
            // 
            // richTextBox1
            // 
            this.richTextBox1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F);
            this.richTextBox1.Location = new System.Drawing.Point(80, 11);
            this.richTextBox1.Name = "richTextBox1";
            this.richTextBox1.Size = new System.Drawing.Size(304, 234);
            this.richTextBox1.TabIndex = 6;
            this.richTextBox1.Text = "";
            // 
            // wiosnaBox
            // 
            this.wiosnaBox.AutoSize = true;
            this.wiosnaBox.Location = new System.Drawing.Point(12, 17);
            this.wiosnaBox.Name = "wiosnaBox";
            this.wiosnaBox.Size = new System.Drawing.Size(62, 17);
            this.wiosnaBox.TabIndex = 10;
            this.wiosnaBox.Text = "Wiosna";
            this.wiosnaBox.UseVisualStyleBackColor = true;
            this.wiosnaBox.CheckedChanged += new System.EventHandler(this.wiosnaBox_CheckedChanged);
            // 
            // latoBox
            // 
            this.latoBox.AutoSize = true;
            this.latoBox.Location = new System.Drawing.Point(12, 40);
            this.latoBox.Name = "latoBox";
            this.latoBox.Size = new System.Drawing.Size(47, 17);
            this.latoBox.TabIndex = 11;
            this.latoBox.Text = "Lato";
            this.latoBox.UseVisualStyleBackColor = true;
            this.latoBox.CheckedChanged += new System.EventHandler(this.latoBox_CheckedChanged);
            // 
            // jesienBox
            // 
            this.jesienBox.AutoSize = true;
            this.jesienBox.Location = new System.Drawing.Point(12, 63);
            this.jesienBox.Name = "jesienBox";
            this.jesienBox.Size = new System.Drawing.Size(56, 17);
            this.jesienBox.TabIndex = 12;
            this.jesienBox.Text = "Jesien";
            this.jesienBox.UseVisualStyleBackColor = true;
            this.jesienBox.CheckedChanged += new System.EventHandler(this.jesienBox_CheckedChanged);
            // 
            // zimaBox
            // 
            this.zimaBox.AutoSize = true;
            this.zimaBox.Location = new System.Drawing.Point(12, 87);
            this.zimaBox.Name = "zimaBox";
            this.zimaBox.Size = new System.Drawing.Size(49, 17);
            this.zimaBox.TabIndex = 13;
            this.zimaBox.Text = "Zima";
            this.zimaBox.UseVisualStyleBackColor = true;
            this.zimaBox.CheckedChanged += new System.EventHandler(this.zimaBox_CheckedChanged);
            // 
            // tempZewText
            // 
            this.tempZewText.Enabled = false;
            this.tempZewText.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.tempZewText.Location = new System.Drawing.Point(718, 11);
            this.tempZewText.Name = "tempZewText";
            this.tempZewText.Size = new System.Drawing.Size(167, 23);
            this.tempZewText.TabIndex = 14;
            this.tempZewText.Text = "Temperatura na zewnątrz:";
            // 
            // aktualnaTempZew
            // 
            this.aktualnaTempZew.AutoSize = true;
            this.aktualnaTempZew.Location = new System.Drawing.Point(891, 16);
            this.aktualnaTempZew.Name = "aktualnaTempZew";
            this.aktualnaTempZew.Size = new System.Drawing.Size(24, 13);
            this.aktualnaTempZew.TabIndex = 15;
            this.aktualnaTempZew.Text = "0°C";
            // 
            // tempWewText
            // 
            this.tempWewText.Enabled = false;
            this.tempWewText.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.tempWewText.Location = new System.Drawing.Point(718, 40);
            this.tempWewText.Name = "tempWewText";
            this.tempWewText.Size = new System.Drawing.Size(167, 23);
            this.tempWewText.TabIndex = 16;
            this.tempWewText.Text = "Temperatura wewnątrz:";
            // 
            // aktualnaTempWew
            // 
            this.aktualnaTempWew.AutoSize = true;
            this.aktualnaTempWew.Location = new System.Drawing.Point(891, 45);
            this.aktualnaTempWew.Name = "aktualnaTempWew";
            this.aktualnaTempWew.Size = new System.Drawing.Size(24, 13);
            this.aktualnaTempWew.TabIndex = 17;
            this.aktualnaTempWew.Text = "0°C";
            // 
            // stalaBox
            // 
            this.stalaBox.AutoSize = true;
            this.stalaBox.Location = new System.Drawing.Point(12, 110);
            this.stalaBox.Name = "stalaBox";
            this.stalaBox.Size = new System.Drawing.Size(52, 17);
            this.stalaBox.TabIndex = 18;
            this.stalaBox.Text = "Stała";
            this.stalaBox.UseVisualStyleBackColor = true;
            this.stalaBox.CheckedChanged += new System.EventHandler(this.stalaBox_CheckedChanged);
            // 
            // aTempWykres
            // 
            this.aTempWykres.AntiAliasing = System.Windows.Forms.DataVisualization.Charting.AntiAliasingStyles.None;
            chartArea5.Name = "ChartArea1";
            this.aTempWykres.ChartAreas.Add(chartArea5);
            legend5.Name = "Legend1";
            this.aTempWykres.Legends.Add(legend5);
            this.aTempWykres.Location = new System.Drawing.Point(12, 277);
            this.aTempWykres.Name = "aTempWykres";
            this.aTempWykres.Palette = System.Windows.Forms.DataVisualization.Charting.ChartColorPalette.None;
            this.aTempWykres.PaletteCustomColors = new System.Drawing.Color[] {
        System.Drawing.Color.Crimson};
            series5.ChartArea = "ChartArea1";
            series5.ChartType = System.Windows.Forms.DataVisualization.Charting.SeriesChartType.Spline;
            series5.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F);
            series5.LabelAngle = 1;
            series5.Legend = "Legend1";
            series5.MarkerSize = 1;
            series5.Name = "TabelaWew";
            this.aTempWykres.Series.Add(series5);
            this.aTempWykres.Size = new System.Drawing.Size(474, 234);
            this.aTempWykres.TabIndex = 19;
            this.aTempWykres.Text = "chart1";
            // 
            // aTempZewWykres
            // 
            this.aTempZewWykres.AntiAliasing = System.Windows.Forms.DataVisualization.Charting.AntiAliasingStyles.None;
            this.aTempZewWykres.BorderlineWidth = 10;
            chartArea6.Name = "ChartArea1";
            this.aTempZewWykres.ChartAreas.Add(chartArea6);
            legend6.Name = "Legend1";
            this.aTempZewWykres.Legends.Add(legend6);
            this.aTempZewWykres.Location = new System.Drawing.Point(492, 277);
            this.aTempZewWykres.Name = "aTempZewWykres";
            this.aTempZewWykres.Palette = System.Windows.Forms.DataVisualization.Charting.ChartColorPalette.None;
            this.aTempZewWykres.PaletteCustomColors = new System.Drawing.Color[] {
        System.Drawing.Color.DodgerBlue};
            series6.ChartArea = "ChartArea1";
            series6.ChartType = System.Windows.Forms.DataVisualization.Charting.SeriesChartType.Spline;
            series6.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F);
            series6.LabelBorderDashStyle = System.Windows.Forms.DataVisualization.Charting.ChartDashStyle.NotSet;
            series6.Legend = "Legend1";
            series6.MarkerBorderWidth = 5;
            series6.MarkerSize = 10;
            series6.Name = "TabelaZew";
            series6.YValuesPerPoint = 4;
            this.aTempZewWykres.Series.Add(series6);
            this.aTempZewWykres.Size = new System.Drawing.Size(423, 234);
            this.aTempZewWykres.TabIndex = 20;
            this.aTempZewWykres.Text = "chart1";
            // 
            // OcieplanyBox
            // 
            this.OcieplanyBox.AutoSize = true;
            this.OcieplanyBox.Checked = true;
            this.OcieplanyBox.CheckState = System.Windows.Forms.CheckState.Checked;
            this.OcieplanyBox.Location = new System.Drawing.Point(511, 63);
            this.OcieplanyBox.Name = "OcieplanyBox";
            this.OcieplanyBox.Size = new System.Drawing.Size(101, 17);
            this.OcieplanyBox.TabIndex = 21;
            this.OcieplanyBox.Text = "Pokoj ocieplany";
            this.OcieplanyBox.UseVisualStyleBackColor = true;
            this.OcieplanyBox.CheckedChanged += new System.EventHandler(this.OcieplanyBox_CheckedChanged);
            // 
            // NieOcieplanyBox
            // 
            this.NieOcieplanyBox.AutoSize = true;
            this.NieOcieplanyBox.Location = new System.Drawing.Point(511, 86);
            this.NieOcieplanyBox.Name = "NieOcieplanyBox";
            this.NieOcieplanyBox.Size = new System.Drawing.Size(127, 17);
            this.NieOcieplanyBox.TabIndex = 22;
            this.NieOcieplanyBox.Text = "Pokoj bez ocieplenia ";
            this.NieOcieplanyBox.UseVisualStyleBackColor = true;
            this.NieOcieplanyBox.CheckedChanged += new System.EventHandler(this.NieOcieplanyBox_CheckedChanged);
            // 
            // Piecyk
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(923, 529);
            this.Controls.Add(this.NieOcieplanyBox);
            this.Controls.Add(this.OcieplanyBox);
            this.Controls.Add(this.aTempZewWykres);
            this.Controls.Add(this.aTempWykres);
            this.Controls.Add(this.stalaBox);
            this.Controls.Add(this.aktualnaTempWew);
            this.Controls.Add(this.tempWewText);
            this.Controls.Add(this.aktualnaTempZew);
            this.Controls.Add(this.tempZewText);
            this.Controls.Add(this.zimaBox);
            this.Controls.Add(this.jesienBox);
            this.Controls.Add(this.latoBox);
            this.Controls.Add(this.wiosnaBox);
            this.Controls.Add(this.richTextBox1);
            this.Controls.Add(this.aktualnaTempPok);
            this.Controls.Add(this.tempPokText);
            this.Controls.Add(this.temperaturaPokojuBar);
            this.Name = "Piecyk";
            this.Text = "Piecyk";
            ((System.ComponentModel.ISupportInitialize)(this.temperaturaPokojuBar)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.aTempWykres)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.aTempZewWykres)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TrackBar temperaturaPokojuBar;
        private System.Windows.Forms.TextBox tempPokText;
        private System.Windows.Forms.Label aktualnaTempPok;
        private System.Windows.Forms.RichTextBox richTextBox1;
        private System.Windows.Forms.CheckBox wiosnaBox;
        private System.Windows.Forms.CheckBox latoBox;
        private System.Windows.Forms.CheckBox jesienBox;
        private System.Windows.Forms.CheckBox zimaBox;
        private System.Windows.Forms.TextBox tempZewText;
        private System.Windows.Forms.Label aktualnaTempZew;
        private System.Windows.Forms.TextBox tempWewText;
        private System.Windows.Forms.Label aktualnaTempWew;
        private System.Windows.Forms.CheckBox stalaBox;
        private System.Windows.Forms.DataVisualization.Charting.Chart aTempWykres;
        private System.Windows.Forms.DataVisualization.Charting.Chart aTempZewWykres;
        private System.Windows.Forms.CheckBox OcieplanyBox;
        private System.Windows.Forms.CheckBox NieOcieplanyBox;
    }
}

